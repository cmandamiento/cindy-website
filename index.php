<?php 
if($_POST['name']){		
	$para  = 'cindy.benavides.zavala@gmail.com';
	$titulo = 'Mensaje desde tu sitio web';
	$mensaje = '
	<html>
	<head>
	  <title>Nuevo Mensaje</title>
	</head>
	<body>
	  <p>Mensaje de: '.$_POST['name'].'</p>
	  <p>Correo: '.$_POST['correo'].'</p>
	  <p>Cuerpo: '.$_POST['contact'].'</p>
	</body>
	</html>
	';
	$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
	$cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	$cabeceras .= 'From: '.$_POST['name'].' <'.$_POST['correo'].'>'."\n";
	mail($para, $titulo, $mensaje,$cabeceras);
}
?>
<!DOCTYPE html>
<!--[if lte IE 8]>               <html class="ie8 no-js" lang="en">    <![endif]-->
<!--[if lte IE 10]>				 <html class="ie10 no-js" lang="en">   <![endif]-->
<!--[if !IE]>-->				 <html class="not-ie no-js" lang="en"> <!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cindy Benavides</title>		
	<link rel="shortcut icon" href="images/favicon.png">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">	
  <link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/grid.css" />
	<link rel="stylesheet" href="css/layout.css" />
	<link rel="stylesheet" href="css/fontello.css" />
	<link rel="stylesheet" href="css/animation.css" />
	<link rel="stylesheet" href="css/compartido.css" />
	<link rel="stylesheet" href="js/layerslider/css/layerslider.css" />
	<link rel="stylesheet" href="js/flexslider/flexslider.css" />
	<link rel="stylesheet" href="js/fancybox/jquery.fancybox.css" />
	<script src="js/jquery.modernizr.js"></script>
</head>
<body data-spy="scroll" data-target="#navigation" class="home">
<a class="player" id="bgndVideo" data-property="{
		videoURL: 'http://www.youtube.com/watch?v=Kr4Sxe9DmdQ',
		containment:'body',
		autoPlay: true,
		quality: 'small',
		mute: true, 
		startAt: 0,
		opacity: 1,
		ratio: '16/9', 
		addRaster: false }">
	</a>
	<div class="loader"></div><!--/ .loader-->
<header id="header" class="transparent">
	<div class="header-in clearfix">
		<h1 id="logo"><img src="./images/logo.png"/></h1>
		<nav id="navigation" class="navigation">
			<ul>
				<li><a href="#home">inicio<p>Hola!</p></a>
				</li>
				<li><a href="#about">sobre mi<p>¿Quién Soy?</p></a></li>
				<li><a href="#portfolio">trabajos<p>Lo que Hago</p></a></li>
				<li><a href="#contact">contacto<p>Encuéntrame</p></a></li>
			</ul>
		</nav><!--/ #navigation-->
	</div><!--/ .header-in-->
</header><!--/ #header-->
<!-- - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - -->
<!-- - - - - - - - - - - - - - Wrapper - - - - - - - - - - - - - - - - -->
<div id="wrapper">
	<section id="home" class="page">
		<section class="section padding-off">
			<div id="layerslider-container">
				<div id="layerslider">
					<div class="ls-layer" style="slidedirection: left; durationin: 1500; durationout: 1500; easingin: easeInOutQuint; timeshift: -500;">
						<img alt="" class="ls-bg" src="./images/2045x9502.jpg">					
					</div><!--/ .ls-layer-->
					<div class="ls-layer" style="slidedirection: right; durationin: 1500; durationout: 1500; easingin: easeInOutQuint; timeshift: -500;">
						<img alt="" class="ls-bg" src="./images/2045x950.jpg">
					</div><!--/ .ls-layer-->
					<div class="ls-layer" style="slidedirection: right; durationin: 1500; durationout: 1500; easingin: easeInOutQuint; timeshift: -500;">
						<img alt="" class="ls-bg" src="./images/2045x9502.jpg">
					</div><!--/ .ls-layer-->
				</div><!--/ #layerslider-->
			</div><!--/ #layerslider-container-->	
			
		</section><!--/ .section-->
	</section><!--/ .page-->
	<!--
	BEGIN SECTION
	-->
	<section class="sobremi page" id="about">
		<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="titulo">
				    	<h1>sobre mi</h1>
							<img src="images/linea.jpg" alt="">
							<h2>¿Quién <p>Soy?</p></h2>
							<img src="images/linea.jpg" alt="">
						</div>
				  </div>
			    <div class="col-xs-4">
		      	<img src="./images/yo.png"/>
			    </div>
			    <div class="col-xs-4">
						<p class="meee">Hola! Soy Cindy Benavides Zavala diseñadora gráfica, les contaré un poco sobre mi...
		        	Desde pequeña siempre me gusto el diseño, y es lo que ahora hago y me encanta, diseñar mas que un trabajo para mi es una pasión. Me fascina el diseño digital y detesto hacer trabajos en 4D, Realmente nunca imagine que el mundo del diseño se convertiriá en algo tan especial para mi…
		          Me considero una persona responsable pero algo desordenada y me encantan los animales.
		          Espero les guste mi portafolio, mis datos se encuentran en contacto, Gracias!!!</p>				    	
			    </div>
			    <div class="col-xs-4">
						<img src="./images/akira.png"/>  	
			    </div>
				</div>
			</div>
		</div>
    <div class="Branding">
    	<div class="BrandingParallax"></div>
    	<div class="container">
    		<div class="row">
			    <div class="col-xs-12">
			    	<h2><b>¿Dónde estudié</b> y que programas uso<b>?</b></h2>
			    	<b>Diseño Gráfico</b> (ISIL - Instituto San Ignacio de Loyola) - <b>Inglés Avanzado</b> (Wright College)
			    	<br>
			    	<b>Diseño y Desarrollo Web</b> (MGP - Master Group Perú)
			    	<br>
			    	<br>
			    	Adobe After Effects - Adobe Illustrator - Adobe Flash - Adobe Indesign - Sublime Text
			    </div>
		    </div>
    	</div>
    </div>
  </section>
 	<!--
	END SECTION
	-->   
	<!--
	BEGIN SECTION
	-->
	<section class="sobremi page" id="portfolio">
		<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="titulo">
				    	<h1>TRABAJOS</h1>
				    	<img src="images/linea.jpg" alt="">
							<h2>Lo que <p>hago</p></h2>
				    	<img src="images/linea.jpg" alt="">
						</div>
				  </div>
					<div class="Portfolio-title">
						<img src="./images/ico-plus.png">Editorial
					</div>
					<!-- BEGIN PORTFOLIO WORKS -->
					<div class="col-xs-4 pss psslarge">
						<div class="portitem">
							<a href="pasos.html"><div class="porthover">
								<img src="./images/mas.png" alt="">
								<br>
								<b>INFOGRAFÍA</b>
								<br>
								Pasos de Gigante
							</div></a>
							<img class="realimage" src="./images/portfolio/trabajosimg1.jpg" alt="">
						</div>
					</div>
					<div class="col-xs-8 collapse">
						<div class="col-xs-4 pss">
							<div class="portitem">
								<img class="realimage" src="./images/portfolio/frase.jpg" alt="">
							</div>
						</div>
						<div class="col-xs-8 pss pssmedium">
							<div class="portitem">
								
								<a href="revista.html"><div class="porthover">
									<img src="./images/mas.png" alt="">
									<br>
									<b>REVISTA</b>
									<br>
									Magazine SpaciOH
								</div></a>
								<img class="realimage" src="./images/portfolio/trabajosimg3.jpg" alt="">
							</div>
						</div>
						<div class="col-xs-4 pss">
							<div class="portitem">
								<a href="paracas.html"><div class="porthover">
									<img src="./images/mas.png" alt="">
									<br>
									<b>SEÑALÉTICA</b>
									<br>
									Manual de Paracas
								</div></a>
								<img class="realimage" src="./images/portfolio/trabajosimg2.jpg" alt="">
							</div>
						</div>
						<div class="col-xs-4 pss">
							<div class="portitem">
								<a href="cd.html"><div class="porthover">
									<img src="./images/mas.png" alt="">
									<br>
									<b>EMPAQUE CD</b>
									<br>
									The Black Keys
								</div></a>
								<img class="realimage" src="./images/portfolio/trabajosimg4.jpg" alt="">
							</div>
						</div>
						<div class="col-xs-4 pss">
							<div class="portitem">
								<a href="pisco.html"><div class="porthover">
									<img src="./images/mas.png" alt="">
									<br>
									<b>INFOGRAFÍA</b>
									<br>
									Pisco Sour
								</div></a>
								<img class="realimage" src="./images/portfolio/trabajosimg5.jpg" alt="">
							</div>
						</div>
					</div>
					<!-- END PORTFOLIO WORKS -->
					<div class="Portfolio-title">
						<img src="./images/ico-plus.png">Branding
					</div>
					<!-- BEGIN PORTFOLIO BRANDING -->
					<div class="col-xs-8 collapse">
						<div class="col-xs-8 pss pssmedium">
							<div class="portitem">
								<div class="porthover">
									<img src="./images/mas.png" alt="">
									<br>
									<b>REDISEÑO WUTHSA</b>
									<br>
									Branding de la marca
								</div>
								<img class="realimage" src="./images/portfolio/trabajosimg11.jpg" alt="">
							</div>
						</div>
						<div class="col-xs-4 pss">
							<div class="portitem">
								<img class="realimage" src="./images/portfolio/frase2.jpg" alt="">
							</div>
						</div>
						<div class="col-xs-4 pss">
							<div class="portitem">
								<div class="porthover">
									<img src="./images/mas.png" alt="">
									<br>
									<b>EVIMED</b>
									<br>
									Centro podólogico
								</div>
								<img class="realimage" src="./images/portfolio/trabajosimg13.jpg" alt="">
							</div>
						</div>
						<div class="col-xs-4 pss">
							<div class="portitem">
								<div class="porthover">
									<img src="./images/mas.png" alt="">
									<br>
									<b>PROPUESTA DE MARCA</b>
									<br>
									Alternativa Wuthsa
								</div>
								<img class="realimage" src="./images/portfolio/trabajosimg14.jpg" alt="">
							</div>
						</div>
						<div class="col-xs-4 pss">
							<div class="portitem">
								<div class="porthover">
									<img src="./images/mas.png" alt="">
									<br>
									<b>MARCA CINDY</b>
									<br>
									Diseño de la marca
								</div>
								<img class="realimage" src="./images/portfolio/trabajosimg12.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="col-xs-4 pss psslarge">
						<div class="portitem">
							<div class="porthover">
								<img src="./images/mas.png" alt="">
								<br>
								<b>HEBSAN</b>
								<br>
								Campaña 360
							</div>
							<img class="realimage" src="./images/portfolio/trabajosimg15.jpg" alt="">
						</div>
					</div>
					<!-- END PORTFOLIO BRANDING -->
					<div class="Portfolio-title">
						<img src="./images/ico-plus.png">Edición de Video y Multimedia
					</div>
					<!-- BEGIN PORTFOLIO MULTIMEDIA-->
					<div class="col-xs-4 pss psslarge">
						<div class="portitem">
							<div class="porthover">
								<img src="./images/mas.png" alt="">
								<br>
								<b>HUGO EL HAMSTER</b>
								<br>
								Animación en Flash
							</div>
								<img class="realimage" src="./images/portfolio/trabajosimg6.jpg" alt="">
						</div>
					</div>
					<div class="col-xs-8 collapse">
						<div class="col-xs-4 pss">
							<div class="portitem">
								<img class="realimage" src="./images/portfolio/frase3.jpg" alt="">
							</div>
						</div>
						<div class="col-xs-8 pss pssmedium">
							<div class="portitem">
								<div class="porthover">
									<img src="./images/mas.png" alt="">
									<br>
									<b>DONA SANGRE</b>
									<br>
									Animación en Flash
								</div>
								<img class="realimage" src="./images/portfolio/trabajosimg7.jpg" alt="">
							</div>
						</div>
						<div class="col-xs-4 pss">
							<div class="portitem">
								<div class="porthover">
									<img src="./images/mas.png" alt="">
									<br>
									<b>PERU S NEXT TOP MODEL</b>
									<br>
									Opening Programa
								</div>
								<img class="realimage" src="./images/portfolio/trabajosimg8.jpg" alt="">
							</div>
						</div>
						<div class="col-xs-4 pss">
							<div class="portitem">
								<div class="porthover">
									<img src="./images/mas.png" alt="">
									<br>
									<b>OCTAVIO PAZ</b>
									<br>
									Animación en After Effect
								</div>
								<img class="realimage" src="./images/portfolio/trabajosimg9.jpg" alt="">
							</div>
						</div>
						<div class="col-xs-4 pss">
							<div class="portitem">
								<div class="porthover">
									<img src="./images/mas.png" alt="">
									<br>
									<b>MUJERES ASESINAS</b>
									<br>
									Comercial en After Effect
								</div>
								<img class="realimage" src="./images/portfolio/trabajosimg10.jpg" alt="">
							</div>
						</div>
					</div>
					<!-- END PORTFOLIO MULTIMEDIA -->

					<div class="Portfolio-title">
						<img src="./images/ico-plus.png">Diseño de Paginas Web
					</div>

					<!-- BEGIN PORTFOLIO WEB DESIGN -->
					<div class="col-xs-8 collapse">
						<div class="col-xs-8 pss pssmedium">
							<div class="portitem">
								<div class="porthover">
									<img src="./images/mas.png" alt="">
									<br>
									<b>BENAVIDES</b>
									<br>
									Primera Pagina
								</div>
								<img class="realimage" src="./images/portfolio/trabajosimg16.jpg" alt="">
							</div>
						</div>
						<div class="col-xs-4 pss">
							<div class="portitem">
								<img class="realimage" src="./images/portfolio/frase4.jpg" alt="">
							</div>
						</div>
						<div class="col-xs-4 pss">
							<div class="portitem">
								<div class="porthover">
									<img src="./images/mas.png" alt="">
									<br>
									<b>DRAG&DROP</b>
									<br>
									Blog de diseño
								</div>
								<img class="realimage" src="./images/portfolio/trabajosimg17.jpg" alt="">
							</div>
						</div>
						<div class="col-xs-4 pss">
							<div class="portitem">
								<div class="porthover">
									<img src="./images/mas.png" alt="">
									<br>
									<b>WUTHSA</b>
									<br>
									Servicios Generales
								</div>
								<img class="realimage" src="./images/portfolio/trabajosimg19.jpg" alt="">
							</div>
						</div>
						<div class="col-xs-4 pss">
							<div class="portitem">
								<div class="porthover">
									<img src="./images/mas.png" alt="">
									<br>
									<b>CINDY BLOG</b>
									<br>
									Blog Wordpress
								</div>
								<img class="realimage" src="./images/portfolio/trabajosimg19.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="col-xs-4 pss psslarge">
						<div class="portitem">
							<div class="porthover">
								<img src="./images/mas.png" alt="">
								<br>
								<b>SABI - SALUD&BIENESTAR</b>
								<br>
								Recetas de comida saludable
							</div>
							<img class="realimage" src="./images/portfolio/trabajosimg20.jpg" alt="">
						</div>
					</div>
					<!-- END PORTFOLIO WEB DESIGN -->
				</div>
			</div>
		</div>
		<div class="Branding BrandingVideo">
			<div id="bgndVideo"></div>
    	<div class="container containerReel">
    		<div class="row">
			    <div class="col-xs-12">
			    	<h1><b>¿Quieres ver</b> mi Reel<b>?</b></h1>
			    	<a href="http://google.com" style="display:block;width:265px;height:40px;margin: 8px auto;color:#fff;background:#542f7d;line-height:40px;">VER REEL</a>
			    </div>
		    </div>
    	</div>			
    </div>
  </section>
 	<!--
	END SECTION
	--> 
 	<!--
	END SECTION
	-->   
	<!--
	BEGIN SECTION
	-->
	<section class="sobremi page" id="contact">
		<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="titulo">
				    	<h1>CONTACTO</h1>
				    	<img src="images/linea.jpg" alt="">
							<h2><i>Encuéntrame</i></h2>
							<img src="images/linea.jpg" alt="">
						</div>
				  </div>
				</div>
				<br>
				<br>
				<div class="row">
			    <div class="col-xs-6">
			    	<img src="./images/contact.jpg">
			    </div>
			    <div class="col-xs-6">
			    	<form action="index.php" method="POST">
			    		<input type="text" name="name" placeholder="Tu nombre" required>
			    		<input type="text" name="correo" placeholder="Tu correo" class="email" required>
			    		<textarea name="contact" id="" rows="10" placeholder="Escribe tu mensaje..." required></textarea>
			    		<input type="submit" value="ENVIAR" class="btn-submit">
			    	</form>
			    	<h4>CONTACTO</h4>
			    	Si te encuentras interesado en algún servicio, por favor siéntete libre
			    	de escribirme un mensaje o llamar a los siguientes números
			    	<div class="row">
				    	<div class="col-xs-5">
				    		<img src="./images/telefono.jpg"  width="60" class="left">
				    		<br>	
				    		+511 952800426
				    		<br>
				    		+511 3480000
				    	</div>
				    	<div class="col-xs-7">
				    		<img src="./images/mailjpg.jpg"  width="60" class="left">
				    		<br>	
				    		cindy@cindybenavides.com
								<br>
								cindy.benavidez.zavala@gmail.com
				    	</div>
			    	</div>
			    </div>
				</div>
			</div>
		</div>
  </section>
 	<!--
	END SECTION
	-->
	<section id="footer">
		<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="footer-inner">
							<a href="#home" class="footer-inner-item"> INICIO</a>
							<span class="footer-inner-item sp">-</span>
							<a href="#about" class="footer-inner-item"> SOBRE MI</a>
							<span class="footer-inner-item sp">-</span>
							<a href="#portfolio" class="footer-inner-item"> TRABAJOS</a>
							<span class="footer-inner-item sp">-</span>
							<a href="#contact" class="footer-inner-item"> CONTACTO</a>
						</div>
						<div class="footer-social">
							<div class="footer-social-item">
								
							<a href="#">
								<img src="./images/facebook.png" width="55">	
							</a>
							</div>
							<div class="footer-social-item">
								
							<a href="#">
								<img src="./images/twitter.png" width="55">	
							</a>
							</div>
							<div class="footer-social-item">
								
							<a href="#">
								<img src="./images/pinterest.png" width="55">	
							</a>
							</div>
							<div class="footer-social-item">
								
							<a href="#">
								<img src="./images/youtube.png" width="55">	
							</a>
							</div>
						</div>
						Copyright &copy; 2013 Cindy Benavides - Diseñado por Cindy Benavides Zavala
					</div>
				</div>
			</div>
		</div>
	</section> 
	<!-- - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - -->
</div><!--/ #wrapper-->
<!-- - - - - - - - - - - - - end Wrapper - - - - - - - - - - - - - - - -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
<![endif]-->
<script src="js/jquery.queryloader2.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.easing.1.3.min.js"></script>
<script src="js/jquery.cycle.all.min.js"></script>
<script src="js/layerslider/js/layerslider.transitions.js"></script>
<script src="js/layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
<script src="js/jquery.mixitup.js"></script>
<script src="js/jquery.mb.YTPlayer.js"></script>
<script src="js/jquery.smoothscroll.js"></script>
<script src="js/flexslider/jquery.flexslider.js"></script>
<script src="js/fancybox/jquery.fancybox.pack.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/jquery.gmap.min.js"></script>
<script src="twitter/jquery.tweet.js"></script>
<script src="js/jquery.touchswipe.min.js"></script>
<script src="js/config.js"></script>
<script src="js/custom.js"></script>

</body>
</html>
